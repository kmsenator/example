<?php
//header('Content-type: text/html; charset=utf-8');
set_time_limit(0);
ini_set('memory_limit', -1);
echo "===================================\n";
echo date("d-m-Y H:i:s",time())." START\n";

$OPF = array(
'PR' => 'Production',
'IM' => 'Imports',
'EX' => 'Exports',
/*'DU' => 'Demand',
'SD' => 'Inventory'*/
);

$CTR = array(
'DZA' => 'Algeria',
'AGO' => 'Angola',
'ARG' => 'Argentina',
'AUS' => 'Australia',
'AUT' => 'Austria',
'AZE' => 'Azerbaijan',
'BHR' => 'Bahrain',
'BGD' => 'Bangladesh',
'BRB' => 'Barbados',
'BLR' => 'Belarus',
'BEL' => 'Belgium',
'BLZ' => 'Belize',
'BOL' => 'Bolivia',
'BRA' => 'Brazil',
'BDI' => 'Brunei',
'BGR' => 'Bulgaria',
'CAN' => 'Canada',
'CHL' => 'Chile',
'CHN' => 'China',
'COL' => 'Colombia',
'CRI' => 'Costa Rica',
'HRV' => 'Croatia',
'CUB' => 'Cuba',
'CYP' => 'Cyprus',
'CZE' => 'Czech Republic',
'DNK' => 'Denmark',
'DOM' => 'Dominican Republic',
'ECU' => 'Ecuador',
'EGY' => 'Egypt',
'SLV' => 'El Salvador',
'GNQ' => 'Equatorial Guinea',
'EST' => 'Estonia',
'FIN' => 'Finland',
'FRA' => 'France',
'FYROM' => 'Macedonia',
'GAB' => 'Gabon',
'GEO' => 'Georgia',
'DEU' => 'Germany',
'GRC' => 'Greece',
'GRD' => 'Grenada',
'GTM' => 'Guatemala',
'GUY' => 'Guyana',
'HTI' => 'Haiti',
'HND' => 'Honduras',
'HKG' => 'Hong Kong',
'HUN' => 'Hungary',
'ISL' => 'Iceland',
'IND' => 'India',
'IDN' => 'Indonesia',
'IRN' => 'Iran',
'IRQ' => 'Iraq',
'IRE' => 'Ireland',
'ITA' => 'Italy',
'JAM' => 'Jamaica',
'JPN' => 'Japan',
'KAZ' => 'Kazakhstan',
'KOR' => 'Korea',
'KWT' => 'Kuwait',
'LVA' => 'Latvia',
'LAJ' => 'Libya',
'LTU' => 'Lithuania',
'LUX' => 'Luxembourg',
'MYS' => 'Malaysia',
'MLT' => 'Malta',
'MEX' => 'Mexico',
'MAR' => 'Morocco',
'MMR' => 'Myanmar',
'NLD' => 'Netherlands',
'NIC' => 'Nicaragua',
'NGA' => 'Nigeria',
'NOR' => 'Norway',
'NZL' => 'New Zealand',
'OMN' => 'Oman',
'PAN' => 'Panama',
'PNG' => 'Papua New Guinea',
'PRY' => 'Paraguay',
'PER' => 'Peru',
'PHL' => 'Philippines',
'POL' => 'Poland',
'PRT' => 'Portugal',
'QAT' => 'Qatar',
'ROU' => 'Romania',
'RUS' => 'Russia',
'ZAF' => 'South Africa',
'SAU' => 'Saudi Arabia',
'SGP' => 'Singapore',
'SVK' => 'Slovakia',
'SVN' => 'Slovenia',
'ESP' => 'Spain',
'SUR' => 'Suriname',
'SWE' => 'Sweden',
'CHE' => 'Switzerland',
'SYR' => 'Syria',
'TAIPEI' => 'Taipei',
'TJK' => 'Tajikistan',
'THA' => 'Thailand',
'TTO' => 'Trinidad',
'TUN' => 'Tunisia',
'TUR' => 'Turkey',
'ARE' => 'United Arab Emirates',
'GBR' => 'United Kingdom',
'UKR' => 'Ukraine',
'URY' => 'Uruguay',
'USA' => 'United States',
'VEN' => 'Venezuela',
'VNM' => 'Vietnam',
'YEM' => 'Yemen'
);
	
$m = new MongoClient("mongo_db_server"); // connect
$db = $m->selectDB("db_name");
MongoCursor::$timeout = -1;
$collection2 = $db->selectCollection("indicators_rally");
$collection2->remove(array('quandl_jodi' => true));
MongoCursor::$timeout = -1;
$collection = $db->selectCollection("jodi_oil");
// $cursor = $collection->find(array('dataset_code'=>'OIL_CRPRKB_RUS'));
MongoCursor::$timeout = -1;
$cursor = $collection->find();
$objs = [];
$rcoef = 0.3;
foreach ($cursor as $doc) {
	$mex0 = explode('_', $doc['dataset_code']);
	//echo substr($mex0[1],2,2)."\n";
	if ( in_array(  substr($mex0[1],2,2), array("PR"/*,"IM","EX"*/)  ) ) {
		$d=0;
		$datem = [];
		foreach ($doc['data'] as $data) {
			
			$datem[$d]["date_time"] = $data[0];
			$datem[$d]["value"] = $data[1];
			$datem[$d]["notes"] = $data[2];
			$d++;
		}
		
		$value = 0;
		for ($k=count($datem)-1;$k>=0;$k--)  {
			 echo $k." ".$doc['dataset_code']."\n";
			$date_time = strtotime($datem[$k]["date_time"]);
			$month = date('m',$date_time);
			$year = date('Y',$date_time);
			$lastday = date('t',$date_time);
			$avg_value = $datem[$k]["value"]/$lastday;

			for ($t=1; $t<=$lastday; $t++) {
				//echo "$k $t------------------------------\n";
				$this_day = mktime(0, 0, 0, $month, $t, $year);
				
				$obj = [];
				$obj['dataset_code'] = $doc['dataset_code'];
				$mex = explode('_', $doc['dataset_code']);
				// echo $OPF[substr($mex[1],2,2)]; echo "\n";
				$obj["type"] = "Crude Oil " . $OPF[substr($mex[1],2,2)];
				$obj["title"] = $CTR[$mex[2]] . " " . $obj["type"];
				$obj["country"] = $CTR[$mex[2]];
				$obj["name"] = $doc['name'];
				$obj["description"] = $doc['description'];

				$obj["date_time"] = new MongoDate( $this_day );
				
				if ($t==1) {
					@$obj["value"] = $avg_value + rand(-$rcoef*$avg_value, $rcoef*$avg_value);
					@$value += $obj["value"];
				} elseif ($t==$lastday) {
					@$obj["value"] = $datem[$k]["value"]-$value;
					$value = 0;
				} else {
					@$obj["value"] = ($datem[$k]["value"]-$value)/($lastday-($t-1)) + rand(-$rcoef*$avg_value, $rcoef*$avg_value);
					@$value += $obj["value"];
				}
				@$obj["month_value"] = $datem[$k]["value"];
				@$obj["notes"] = $datem[$k]["notes"];
				
				if ($t==date('j',$date_time)) { $obj["quandl_point"] = true; } else { $obj["quandl_point"] = false; }
				$obj["quandl_jodi"] = true;
				$objs[] = $obj;
				
				try {
					$collection2->insert($obj);
				}
				catch (MongoCursorException $e) {
					echo "Mongo error message: ".$e->getMessage()."\n";
					echo "Mongo error code: ".$e->getCode()."\n";
				}
			}
			if ($k == 0) {
				$value = 0;
				@$delta=$datem[$k]["value"]-$datem[$k+1]["value"];
				$last = new DateTime($datem[$k]["date_time"]); 
				$today = new DateTime();
				$diff = $last->diff($today);
				// echo "month diff=".$diff->format('%m.%d')." "; echo
				$diff_month = ceil($diff->format('%m.%d'));
				// echo "\n";
				$month_mas = [];
				$month_value = $datem[$k]["value"];
				for ($m=1; $m<=$diff_month; $m++){
					$month_value += $delta;
					if ($month_value < 0) { $month_value = 0; }
					// echo 
					$month_mas[$m] = $month_value; 
					// echo "\n";
				}
				for ($m=1; $m<=$diff_month; $m++){
					$valueo = $month_mas[$m];
					$date_other = mktime(0, 0, 0, $month+$m, 1, $year);
					// echo 
					$lastday = date('t',$date_other); 
					// echo "\n";
					// echo 
					$montho = date('m',$date_other); 
					// echo "\n";
					// echo 
					$yearo = date('Y',$date_other); 
					// echo "\n";				
					$avg_value = $valueo/$lastday;
					if ( date('01-m-Y') == date('d-m-Y',$date_other) ) { $lastday = date('j'); }
					for ($t=1; $t<=$lastday; $t++) {
						// echo "$k $t------------------------------\n";
						$this_day = mktime(0, 0, 0, $montho, $t, $yearo);
						
						$obj = [];
						$obj['dataset_code'] = $doc['dataset_code'];
						$mex = explode('_', $doc['dataset_code']);
						// echo $OPF[substr($mex[1],2,2)]; echo "\n";
						$obj["type"] = "Crude Oil " . $OPF[substr($mex[1],2,2)];
						$obj["title"] = $CTR[$mex[2]] . " " . $obj["type"];
						$obj["country"] = $CTR[$mex[2]];
						$obj["name"] = $doc['name'];
						$obj["description"] = $doc['description'];

						$obj["date_time"] = new MongoDate( $this_day );
						
						if ($t==1) {					
							@$obj["value"] = $avg_value + rand(-$rcoef*$avg_value, $rcoef*$avg_value);
							@$value += $obj["value"];
						} elseif ($t==$lastday) {
							@$obj["value"] = $valueo-$value;
							$value = 0;
						} else {
							@$obj["value"] = ($valueo-$value)/($lastday-($t-1)) + rand(-$rcoef*$avg_value, $rcoef*$avg_value);
							@$value += $obj["value"];
						}
						@$obj["month_value"] = $valueo;
						@$obj["notes"] = $datem[$k]["notes"];
						
						$obj["quandl_point"] = false;
						$obj["quandl_jodi"] = true;
						$objs[] = $obj;
						
						try {
							$collection2->insert($obj);
							echo "Insert extropolation\n";
						}
						catch (MongoCursorException $e) {
							echo "Mongo error message: ".$e->getMessage()."\n";
							echo "Mongo error code: ".$e->getCode()."\n";
						}
					} //day
				} //month
			}
			
			//if ($d==2) {
				// var_dump($objs);
				// die();
			// }
			$d++;
		}		
	} //if array
}
echo date("d-m-Y H:i:s",time())." END\n";
?>
