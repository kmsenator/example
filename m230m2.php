<?php define('CMS',0);
ini_set('display_errors',1);
error_reporting(E_ALL);
header('Content-type: text/html; charset=utf-8');

include_once('conf.php');
include_once('db.php');
include_once('user.php');
include_once('module.php');
include_once('function.php');
include_once '../mod/cabinet/lib/odbc.db.php';


$db = new CDatabase(cmsDB_Server,cmsDB_User,cmsDB_Password, cmsDB_Name,'');
if(!$db->Connect()){
	echo($db->GetErrorHTML());
}

$mdb = new CDatabase(cmsDB_Server,cmsDB_User,cmsDB_Password, cmsDB_Name,'');
$mdb = new COdbcDatabase();
$mdb->Type = DB_MSSQL;
$mdb->Server = cmsMDB_Server;
$mdb->Database = cmsMDB_Name;
$mdb->User = cmsMDB_User;
$mdb->Password = cmsMDB_Password;


if(!$mdb->Connect()){
	echo("Not connect to MS SQL Server from ODBC<br><br>Error number: {$mdb->_errnum}<br>Message: {$mdb->_errmsg}<br><br>");
}
		/*date('m',time())-1=7 - 1 7-6=1
		                   8 - 2 8-6=2
						   9 - 3 9-6=3
						   10- 4 10-6=4...*/
		$month_t = 8;
		$year = 2018;
		$time = mktime(4, 30, 0, $month_t /*date('m',time())*/, 1, $year);//time();
		if (isset($_GET['ds'])) {
			$ds = $_GET['ds'];
		} else {
			 $ds = date('Ym01',$time);
			//$ds = date('Ymd',time()-60*60*24);
		}
/* v dekabre Ym29, f nado Ymt */
		if (isset($_GET['de'])) {
			$de = $_GET['de'];
		} else {
			 $de = date('Ymt',$time);
			//$de = date('Ymd',time()-60*60*24);
		}
		
		if (isset($_GET['baza'])) $baza = $_GET['baza']; else $baza = 2111;//
		
		$time1 = strtotime($ds);
		$time2 = strtotime($de);

		//echo 
		$query = "SELECT a.[unit_id], a.[start_date], dateadd(month, 1, a.[start_date]) as end_date, a.[start_time], a.p_all as p_start, (a.p_all+b.sum_dp_all) as p_end, b.sum_dp_all, b.electro, b.sum_dp_all*(1+b.kp/100) electro_poter, b.kt, b.kp, b.point_id, b.type_id, b.name, b.address, b.device_serial, b.[input]
					FROM

						(SELECT [unit_id],[start_date],[start_time],[p_all],[enabled]
						FROM [pribordb].[dbo].[report_electro_config]
						where [enabled]=1) a
						
					INNER JOIN
						
						(/*SELECT ea.unit_id, sum(dp_all) sum_dp_all, ec.kt, ec.kp, sum(dp_all*ec.kt*(1+ec.kp/100)) as electro, ec.point_id, ec.type_id, f.name, ea.device_serial, f.address, ec.[input]
							FROM [DataCollectorMS].[dbo].[electro_archive] ea
							INNER JOIN [DataCollectorMS].[dbo].[electro_config] ec ON ea.unit_id = ec.unit_id
							INNER JOIN [DataCollectorMS].[dbo].[units] u ON u.id = ea.unit_id
							INNER JOIN [DataCollectorMS].[dbo].[facilites] f ON f.[id] = u.[facility_id]
							INNER JOIN [DataCollectorMS].[dbo].[clients] c ON c.[id] = f.[client_id]
							WHERE device_time IS NULL AND ec.kp IS NOT NULL AND ec.kp IS NOT NULL
							AND response_time BETWEEN '".date('Ymd H:i:s',mktime(4, 30, 0, date('m',$time1), date('d',$time1), date('Y',$time1)))."' AND '".date('Ymd H:i:s',mktime(4, 0, 0, date('m',$time2), date('d',$time2)+1, date('Y',$time2)))."'
							AND ea.unit_id IN (223,224)
							GROUP BY ea.unit_id, ec.kt, ec.kp, ec.point_id, ec.type_id, f.name, ea.device_serial, f.address, ec.[input]
						
						UNION*/
						
						SELECT ecl.unit_id, sum([Pp]/2) sum_dp_all, ec.kt, ec.kp, sum(([Pp]/2)*ec.kt*(1+ec.kp/100)) as electro, ec.point_id, ec.type_id, n.title+'' as 'name', ecl.device_serial, ec.address, ec.[input]
							FROM [192.168.4.82].[LERS].[dbo].[ElectricPower] m
							INNER JOIN [DataCollectorMS].[dbo].[electro_config_lers] ecl ON m.[MeasurePointId] = ecl.mpid
							INNER JOIN [DataCollectorMS].[dbo].[electro_config] ec ON ecl.unit_id = ec.unit_id
							INNER JOIN [192.168.4.82].[LERS].[dbo].[MeasurePoint] mp ON mp.[MeasurePoint_ID] = m.[MeasurePointId]
							INNER JOIN [192.168.4.82].[LERS].[dbo].[Node] n ON n.id = mp.[MeasurePoint_NodeID]
							/*INNER JOIN [192.168.4.82].[LERS].[dbo].[DeviceMeasurePoint] dmp ON dmp.[MeasurePointId] = m.[MeasurePointId]
							INNER JOIN [192.168.4.82].[LERS].[dbo].[Complex] c ON c.[Complex_ID] = dmp.DeviceID*/
							WHERE [DataDate] BETWEEN '".date('Ymd H:i:s',mktime(4, 30, 0, date('m',$time1), date('d',$time1), date('Y',$time1)))."' AND '".date('Ymd H:i:s',mktime(4, 0, 0, date('m',$time2), date('d',$time2)+1, date('Y',$time2)))."'
							AND m.MeasurePointID IN (41,40,51,52,1291,81,20,46,47,45,48,25,/*11,*/1279, 1341, 1342)
							AND [Interval] = 30
							GROUP BY ecl.unit_id, ec.kt, ec.kp, ec.point_id, ec.type_id, n.title, ecl.device_serial, ec.address, ec.[input]
						) b
						
				ON a.unit_id = b.unit_id
						
				ORDER BY b.name, b.address, b.[input]";
		//die();
		$mdb->SetQuery($query);
		$ot = $mdb->ExeObjectList();
		$c = count($ot);
		for($i=0;$i<$c;$i++)
		{
			$p = $ot[$i];
			$ot[$i] = $p;
		}
		$otchet=$ot;
				
		//echo '<pre>'; print_r( $otchet); echo '</pre>';				
		


$mdb->Close();
$db->Close(); 
if ($c > 0) {
/* Обнуление и задание переменных */
	$dp = 0;

/* HTML */
echo "<html>\n<head>\n\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">\n</head>\n<body>\n";

echo "<style>
table
{
border-collapse:collapse;
}

.border1 table, .border1 th, .border1 td
{
border: 1px solid black;
padding:2px 10px 2px 10px;
font-size:11px;
font-family: \"Times New Roman\", Times, serif;
}

.border4 table, .border4 th, .border4 td
{
padding:2px;
font-size:9px;
}

.border0 table, .border0 th, .border0 td
{
border: 0px solid black;padding:2px;
font-size:11px;
font-family: \"Times New Roman\", Times, serif;
}

.border5 table tr table, .border5 td tr table td
{
border: 0px solid black;padding:0px;
font-size:11px;
font-family: \"Times New Roman\", Times, serif;
}

p
{
font:11px \"Times New Roman\", Times, serif;
padding-left:15px;
}
</style>\n";

/** PHPExcel_IOFactory */
		require_once 'Classes/PHPExcel/IOFactory.php';

		$inputFileName = "template_integral.xls";
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);

		$objReader = PHPExcel_IOFactory::createReader($inputFileType);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel = $objReader->load($inputFileName);
		
		$objPHPExcel->getProperties()->setCreator(/*iconv('utf-8','windows-1251',*/'АО "Теплоэнерго"'/*)*/);
		$objPHPExcel->getProperties()->setTitle(/*iconv('utf-8','windows-1251',*/"СН2 интегральный"/*)*/);
		$objPHPExcel->getProperties()->setSubject(iconv('utf-8','windows-1251',"СН2 интегральный"));
		$objPHPExcel->getProperties()->setDescription(iconv('utf-8','windows-1251',"СН2 интегральный за ").date('Y-m-d H:i',mktime(4, 30, 0, date('m',$time1), date('d',$time1), date('Y',$time1)))." - ".date('Y-m-d H:i',mktime(4, 0, 0, date('m',$time2), date('d',$time2)+1, date('Y',$time2))));

		$objPHPExcel->setActiveSheetIndex(0);

/*
* Теплосистема 
*/
//echo "<h4>Фактические почасовые объемы покупки электрической энергии, отпускаемых на уровне напряжения _______ договора №________</h4>\n";

echo "<table border='0' width='100%' class='border0'>\n";
echo "<tr><td width='115' style='padding-left:15px;'>Код абонента: </td><td width='200'>5555x_9x</td><td width='100'>Единица измерения: </td><td>кВт·ч</td></tr>\n";
echo "<tr><td style='padding-left:15px;'>Дата создания: </td><td>".date('Y-m-d H:i',time())."</td><td>Пользователь: </td><td>teploenergo_robot</td></tr>\n";
echo "<tr><td style='padding-left:15px;'>Период: </td><td>".date('Y-m-d 00:00',strtotime($ds))."</td><td>&nbsp; </td><td>".date('Y-m-d H:i',mktime(0, 0, 0, date('m',$time2), date('d',$time2)+1, date('Y',$time2)))."</td></tr>\n";
echo "</table>\n";

echo "<table border='0' width='100%' class='border1'>\n";

	echo "\t<tr>\n";	
	echo "\t\t<td>№</td>\n";	
	echo "\t\t<td>Адрес ТУ</td>\n";
	echo "\t\t<td>Точка учета</td>\n";
	echo "\t\t<td>Номер счетчика</td>\n";
	echo "\t\t<td>Кт</td>\n";
	echo "\t\t<td>Кр</td>\n";
	echo "\t\t<td>Дата на начало</td>\n";
	echo "\t\t<td>Показания</td>\n";
	echo "\t\t<td>Дата на конец</td>\n";
	echo "\t\t<td>Показания</td>\n";
	echo "\t\t<td>Разность</td>\n";
	echo "\t\t<td>С учетом потерь</td>\n";
	echo "\t</tr>\n";

	function add_date($givendate,$day=0,$mth=0,$yr=0) {
		$cd = strtotime($givendate);
		$newdate = date('Y-m-d H:i', mktime(date('H',$cd),
		date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
		date('d',$cd)+$day, date('Y',$cd)+$yr));
		return $newdate;
	 }
	 
	for($i=0;$i<$c;$i++)
	{	
		if (trim($otchet[$i]->point_id)==161164 && $i==0) {
			if (mb_strlen($otchet[$i]->input)) $input_addr=", ".iconv('windows-1251','utf-8',$otchet[$i]->input); else $input_addr="";
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.(6+$i), iconv('windows-1251','utf-8',$otchet[$i]->name));
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.(6+$i), iconv('windows-1251','utf-8',$otchet[$i]->address).$input_addr);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.(6+$i), $otchet[$i]->point_id);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.(6+$i), $otchet[$i]->device_serial);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.(6+$i), $otchet[$i]->kt);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.(6+$i), $otchet[$i]->kp);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.(6+$i), date('Y-m-d H:i',strtotime($otchet[$i]->start_date)));
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.(6+$i), number_format($otchet[$i]->p_all, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.(6+$i), (add_date($otchet[$i]->start_date,date('t',$time1),0,0)));
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.(6+$i), number_format($otchet[$i]->p_end, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.(6+$i), number_format($otchet[$i]->sum_dp_all, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.(6+$i), number_format($otchet[$i]->electro, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('B7', "Теле2");
			$objPHPExcel->getActiveSheet()->SetCellValue('B8', "Итого (Котельная 1 минус субабонент Теле2)");
			$objPHPExcel->getActiveSheet()->SetCellValue('M7', number_format($baza, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('M8', number_format($otchet[$i]->electro-$baza, 6, '.', ''));
		} else {
			if (mb_strlen($otchet[$i]->input)) $input_addr=", ".iconv('windows-1251','utf-8',$otchet[$i]->input); else $input_addr="";
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.(8+$i), iconv('windows-1251','utf-8',$otchet[$i]->name));
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.(8+$i), iconv('windows-1251','utf-8',$otchet[$i]->address).$input_addr);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.(8+$i), $otchet[$i]->point_id);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.(8+$i), $otchet[$i]->device_serial);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.(8+$i), $otchet[$i]->kt);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.(8+$i), $otchet[$i]->kp);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.(8+$i), date('Y-m-d H:i',strtotime($otchet[$i]->start_date)));
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.(8+$i), number_format($otchet[$i]->p_all, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.(8+$i), (add_date($otchet[$i]->start_date,date('t',$time1),0,0)));
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.(8+$i), number_format($otchet[$i]->p_end, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.(8+$i), number_format($otchet[$i]->sum_dp_all, 6, '.', ''));
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.(8+$i), number_format($otchet[$i]->electro, 6, '.', ''));
		}
		
		
		echo "\t<tr>\n";
		echo "\t\t<td>".($i+1)."</td>\n";	
		echo "\t\t<td>".iconv('windows-1251','utf-8',$otchet[$i]->name)./*", ".iconv('windows-1251','utf-8',$otchet[$i]->address).*/$input_addr."</td>\n";
		echo "\t\t<td>".@$otchet[$i]->point_id."</td>\n";
		echo "\t\t<td>".@$otchet[$i]->device_serial."</td>\n";
		echo "\t\t<td>".@$otchet[$i]->kt."</td>\n";
		echo "\t\t<td>".@$otchet[$i]->kp."</td>\n";
		echo "\t\t<td>".date('Y-m-d H:i',strtotime($otchet[$i]->start_date))."</td>\n";
		echo "\t\t<td>".number_format($otchet[$i]->p_all, 6, ',', '')."</td>\n";
		echo "\t\t<td>".(add_date($otchet[$i]->start_date,date('t',$time1),0,0))."</td>\n";
		echo "\t\t<td>".number_format($otchet[$i]->p_end, 6, ',', '')."</td>\n";
		echo "\t\t<td>".number_format($otchet[$i]->sum_dp_all, 6, ',', '')."/".number_format(round($otchet[$i]->p_end,3)-($otchet[$i]->p_all), 3, ',', '')."</td>\n";
		
		echo "\t\t<td>".number_format($otchet[$i]->electro, 6, ',', '')."/".number_format(round(round($otchet[$i]->p_end,3)-($otchet[$i]->p_all),3)*$otchet[$i]->kt*(1+$otchet[$i]->kp/100), 3, ',', '')."</td>\n";
		echo "\t</tr>\n";		
	}

echo "</table>\n";


echo "\n</body></html>";
	
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', '---');
	$objPHPExcel->getActiveSheet()->SetCellValue('C2', date('Y-m-d H:i',time()));
	$objPHPExcel->getActiveSheet()->SetCellValue('E2', 'АО "Теплоэнерго"');
	$objPHPExcel->getActiveSheet()->SetCellValue('C3', date('Y-m-d H:i',mktime(0, 0, 0, date('m',$time1), date('d',$time1), date('Y',$time1)))."                                 ".date('Y-m-d H:i',mktime(0, 0, 0, date('m',$time2), date('d',$time2)+1, date('Y',$time2))));
	$objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('G5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('H5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('J5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('K5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('L5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('M5')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('N5')->getFont()->setBold(true);
	
	$objPHPExcel->getActiveSheet()->getStyle('C21');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save(str_replace('.php', '.xls', "xls2/new_int_".$month_t.'_'.date('Y',$time1).".xls"));
}
?>