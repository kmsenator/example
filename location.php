<?php
header('Content-type: text/html; charset=utf-8');
set_time_limit(0);
function google_location($place) {
	sleep(rand(0.3,1));
	$place = trim($place);
	$inputJSON = @file_get_contents("http://maps.google.com/maps/api/geocode/json?language=en&address=".urlencode($place));
	$aplace = array(
				"name"=>null,
				"lat"=>null,
				"lng"=>null
			);
	if (strlen($inputJSON) == 0) {
		echo "Sleep 2 sec\n";
		sleep(2);
		google_location($place);
	} else {
		$input = json_decode( $inputJSON, TRUE ); //convert JSON into array
		if ($input['status'] == "OK") {
			$lname = null;
			$cname = null;
			$pname = null;
			if (array_key_exists(0, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][0]['types'])) {
					if ($input['results'][0]['address_components'][0]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][0]['long_name'];
					} elseif ($input['results'][0]['address_components'][0]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}					
				}
				$pname = $input['results'][0]['address_components'][0]['long_name'];
			}
			if (array_key_exists(1, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][1]['types'])) {
					if ($input['results'][0]['address_components'][1]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][1]['long_name'];
					} elseif ($input['results'][0]['address_components'][1]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			if (array_key_exists(2, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][2]['types'])) {
					if ($input['results'][0]['address_components'][2]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][2]['long_name'];
					} elseif ($input['results'][0]['address_components'][2]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			if (array_key_exists(3, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][3]['types'])) {
					if ($input['results'][0]['address_components'][3]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][3]['long_name'];
					} elseif ($input['results'][0]['address_components'][3]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			if (array_key_exists(4, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][4]['types'])) {
					if ($input['results'][0]['address_components'][4]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][4]['long_name'];
					} elseif ($input['results'][0]['address_components'][4]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			if (array_key_exists(5, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][5]['types'])) {
					if ($input['results'][0]['address_components'][5]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][5]['long_name'];
					} elseif ($input['results'][0]['address_components'][5]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			if (array_key_exists(6, $input['results'][0]['address_components'])) {
				if (array_key_exists(0,$input['results'][0]['address_components'][6]['types'])) {
					if ($input['results'][0]['address_components'][6]['types'][0] == "country") {
						$lname = $input['results'][0]['address_components'][6]['long_name'];
					} elseif ($input['results'][0]['address_components'][6]['types'][0] == "locality") {
						$cname = $input['results'][0]['address_components'][0]['long_name'];
					}
				}
			}
			$lname = str_replace(".","",$lname);
			$aplace = array(
				"place_name"=>$pname,
				"city_name"=>$cname,
				"country_name"=>$lname,
				"lat"=>@$input['results'][0]['geometry']['location']['lat'],
				"lng"=>@$input['results'][0]['geometry']['location']['lng']
			);
			//print_r($input);
			echo "GOOGLE place='".$pname."', city='".$cname."', country='".$lname."' [".$input['results'][0]['geometry']['location']['lat'].", ".$input['results'][0]['geometry']['location']['lng']."]\n";
		} elseif ($input['status'] == "OVER_QUERY_LIMIT") {	
			//print_r($input);
			if ($input['error_message'] == 'You have exceeded your daily request quota for this API. We recommend registering for a key at the Google Developers Console: https://console.developers.google.com/apis/credentials?project=_') {
				echo "$place [ OVER_QUERY_LIMIT ]\n";
				echo " ".date("Y-m-d H:i:s",time())."\n";
				die("EXIT SCRIPT\n");
			} else {
				echo "$place [ OVER SLEEP ]\n";
				echo "Sleep 2 sec\n";
				sleep(2);
				google_location($place);
			}			
		} elseif ($input['status'] == "ZERO_RESULTS") {
			echo "$place [ ZERO ]\n";
		}
	}
	return $aplace;
}

$location = "Москва";
$array_place = google_location($location);
$obj = [];
@$obj['place'] = $array_place['place_name'];
@$obj['city'] = $array_place['city_name'];
@$obj['country'] = $array_place['country_name'];
@$obj['loc'] = [$array_place['lat'],$array_place['lng']];
var_dump($obj);
?>
