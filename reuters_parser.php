<?php
header('Content-type: text/html; charset=utf-8');
set_time_limit(0);
echo "===================================\n";
echo date("d-m-Y H:i:s",time())." START\n";
include('simplehtmldom_1_5/simple_html_dom.php');

function file_contents_exist($url, $response_code = 200)
{
    $headers = get_headers($url);
//echo substr($headers[0], 9, 3);
//echo "\n";
    if (substr($headers[0], 9, 3) == $response_code || substr($headers[0], 9, 3) == 301)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

$m = new MongoClient("mongo_db_server"); // connect
$db = $m->selectDB("db_name");
$collection = $db->selectCollection("urls");
echo $collection->count(array('source' => 'reuters', 'status' => false));echo "\n";
$cursor = $collection->find(array('source' => 'reuters', 'status' => false));
$collection3 = $db->selectCollection("content");
foreach ($cursor as $doc) {
	
	echo date("d-m-Y H:i:s",time())."\n";
	echo $url = trim($doc['url']);
	echo "\n";
	IF ($collection3->count(array('public_url'=>$url))) {
		echo "uge est - $url\n";
		$collection->remove(array('url'=>$url));
	} ELSE { 
		if(file_contents_exist($url)) {
			$html = file_get_html("$url");
			//echo count($html);
			if (mb_strlen($html)>0) {

				$title = '';
				if (count($html->find('h1.article-headline'))) $title = $html->find('h1.article-headline',0)->plaintext;
				$author = ''; $author_link = '';
				if (count($html->find('span.byline'))) {
					$author = trim(str_ireplace("By ", "", $html->find('span.byline',0)->plaintext));
					if (count($html->find('span.byline',0)->find('a'))) $author_link = "http://www.reuters.com".$html->find('span.byline',0)->find('a',0)->href;
				}
				$mdate = ''; if (count($html->find('span.timestamp'))) $mdate = $html->find('span.timestamp',0)->plaintext;
				$date = date("Y-m-d\TH:i:s\Z",strtotime($mdate));
				$text = ''; $html_text = '';
				if (count($html->find('div.column1'))) {
					if (count($html->find('div.column1',0)->find('span'))) {
						$text = trim($html->find('div.column1',0)->find('span',0)->plaintext);
						$html_text = trim($html->find('div.column1',0)->find('span',0));
					}
				}
				$size = mb_strlen($text);
				$theme = ''; if (count($html->find('span.article-section'))) $theme = trim($html->find('span.article-section',0)->plaintext);
				$related = ''; $related_link = '';
				if (count($html->find('div.related-topics'))) {
					if (count($html->find('div.related-topics',0)->find('a'))) {
						$related = trim(str_ireplace(",", "", $html->find('div.related-topics',0)->find('a',0)->plaintext));
						$related_link = "http://www.reuters.com".$html->find('div.related-topics',0)->find('a',0)->href;
						if (count($html->find('div.related-topics',0)->find('a'))>1) {
							$related2 = trim(str_ireplace(",", "", $html->find('div.related-topics',0)->find('a',1)->plaintext));
							$related_link2 = "http://www.reuters.com".$html->find('div.related-topics',0)->find('a',1)->href;
									}
									if (count($html->find('div.related-topics',0)->find('a'))>2) {
							$related3 = trim(str_ireplace(",", "", $html->find('div.related-topics',0)->find('a',2)->plaintext));
										$related_link3 = "http://www.reuters.com".$html->find('div.related-topics',0)->find('a',2)->href;
						}
					}
				}
				$photo_link = ''; $photo_name = ''; $photo_author = '';
				if (count($html->find('div.related-photo-container'))) {
					if (count($html->find('div.related-photo-container',0)->find('img'))) {
						$photo_link = $html->find('div.related-photo-container',0)->find('img',0)->src;
						$photo_name = trim($html->find('div.related-photo-caption',0)->plaintext);
						$photo_author = trim($html->find('div.related-photo-credit',0)->plaintext);
					}
				}
				$is_photo = (strlen($photo_link)>0)?true:false;


				$obj = array();
				$obj['name'] = $title;
				$obj['author'] = $author;
				$obj['author_link'] = $author_link;
				$obj['created_at'] = new MongoDate(strtotime($date));

				$obj['theme'] = $theme;
				$obj['related'] = $related;
				$obj['related_link'] = $related_link;

				if (@!is_null($related2)) {
					$obj['related2'] = $related2;
					$obj['related_link2'] = $related_link2;
				}

				if (@!is_null($related3)) {
				$obj['related3'] = $related3;
				$obj['related_link3'] = $related_link3;
				}

				$obj['is_photo'] = $is_photo;
				$obj['photo_link'] = $photo_link;
				$obj['photo_name'] = $photo_name;
				$obj['photo_author'] = $photo_author;
				$obj['is_downloaded'] = true;
				$obj['public_url'] = $url;
				$obj['content_type'] = "text/plain";
				$obj['is_image'] = false;
				$obj['type'] = "html";
				$obj['plain_text'] = $text;
				$obj['html_text'] = $html_text;
				$obj['content_size'] = $size;
				$obj['processed'] = false;
				$obj['source'] = "reuters_us";
				
				if (strlen(trim($obj['name'])) > 0) {
					$obj['url_error'] = false;
					$obj['enrich'] = true;
				} else {
					$obj['url_error'] = true;
					$obj['enrich'] = false;
				}
				//var_dump($obj);
				$m2 = new MongoClient("mongo_db_server"); // connect
				$db2 = $m2->selectDB("db_name");
				$collection2 = $db2->selectCollection("content");
				try {
					$collection2->insert($obj);
					$collection->update(array("_id" => $doc['_id']), array('$set' => array("status" => true)));
				}
				catch (MongoCursorException $e) {
					echo "Mongo error message: ".$e->getMessage()."\n";
					echo "Mongo error code: ".$e->getCode()."\n";
				}
			} else {echo "Content empty :(\n";}
		} else {
			$obj = array();
			$obj['public_url'] = $url;
			$obj['processed'] = false;
			$obj['source'] = "reuters_us";
			$obj['enrich'] = false;
			$obj['url_error'] = true;
			
			$m2 = new MongoClient("mongo_db_server"); // connect
			$db2 = $m2->selectDB("db_name");
			$collection2 = $db2->selectCollection("content");
			try {
				$collection2->insert($obj);
				$collection->update(array("_id" => $doc['_id']), array('$set' => array("status" => true)));
			}
			catch (MongoCursorException $e) {
				echo "Mongo error message: ".$e->getMessage()."\n";
				echo "Mongo error code: ".$e->getCode()."\n";
			}
		}
	}
}
echo date("d-m-Y H:i:s",time())." END\n";
?>
